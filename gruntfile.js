const mozjpeg = require('imagemin-mozjpeg');

module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    log: {
      foo: [ 1, 2, 3, 4 ],
      bar: "Hello",
      boo: true
    },
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'css/',
          src: ['*.css', '!*.min.css'],
          dest: 'css/',
          ext: '.min.css'
        }]
      }
    },
    uglify: {
      options: {
        mangle: false
      },
      my_target: {
        files: {
          'js/output.min.js': ['js/input.js', 'js/input2.js']
        }
      }
    },
    concat: {
      options: {
        separator: ";",
        stripBanners: true,
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> = ' + '<%= grunt.template.today("yyyy-mm-dd") %> */',
      },
      dist: {
        src: ['js/input.js', 'js/input2.js'],
        dest: 'js/output.js',
      }
    },
    sass: {// Task 
      dist: {// Target 
        options: {                       // Target options 
          style: 'expanded'
        },
        files: {                         // Dictionary of files 
          'cs/style.converted.css': 'css/style.scss',       // 'destination': 'source' 
          'cs/example.converted.css': 'css/example.scss'
        }
      }
    },
    uncss: {
      dist: {
        files: {
          'calculator/destination.css': ['calculator/index.html'] //They will use link stylesheets in the html file
        }
      }
    },
    imagemin: {
      // static: {
      //   options: {
      //     optimizationLevel: 3,
      //     svgoPlugins: [{removeViewBox: false}],
      //     use: [mozjpeg()] // Example plugin usage 
      //   },
      //   files: {
      //     'images/20_remove-icon.min.png': 'images/20_remove-icon.png'
      //   }
      // },
      dynamic: {
        options: {
          optimizationLevel: 3,
          svgoPlugins: [{removeViewBox: false}],
          use: [mozjpeg()] // Example plugin usage 
        },
        files: [{
          expand: true,
          cwd: 'images/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'dest/'
        }]
      }
    },
      // This is optional! 
    notify_hooks: {
      options: {
        enabled: true,
        max_jshint_notifications: 5, // maximum number of notifications from jshint output 
        title: "Grunt Example", // defaults to the name in package.json, or will use project directory's name 
        success: false, // whether successful grunt executions should be notified automatically 
        duration: 3 // the duration of notification in seconds, for `notify-send only 
      }
    },
    watch: {
      cssmin: {
        files: ['css/*.css'],
        tasks: ['cssmin']
      },
      options: {
        title: 'Task Complete',  // optional 
        message: 'SASS finished running', //required 
      }
    }
  });
  //Initialised Grunt tasks
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-notify');
  //Initialised 3rd party Grunt tasks
  grunt.loadNpmTasks('grunt-uncss');

  grunt.registerTask('default', [
    'sass',
    'notify:default'
  ]); //running `grunt default` runs this array of packages

  grunt.registerMultiTask('log', 'Log stuff', function () {
    grunt.log.writeln(this.target + ":" + this.data);
  });

  grunt.registerTask('fool', 'Simple preprocessing', function () {
    if (arguments.length === 0) {
      grunt.log.writeln(this.name + ": no argument passed!");
    } else {
      //grunt fool:<var/>
      grunt.log.writeln(this.name + ":" + this.data)
    }
  });

  // grunt.tasks.run('notify_hooks');
};